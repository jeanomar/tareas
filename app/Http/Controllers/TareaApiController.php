<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarea;
use App\Http\Requests\TareaRequest;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Info(
 *     title="My First API",
 *     version="0.1"
 * )
 */
class TareaApiController extends Controller
{

    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="Permite obtener un token de acceso mediante el envio del email y el password",
     *     tags={"users"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Datos del formulario",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(property="email", type="string", description="Correo electrónico del usuario"),
     *                 @OA\Property(property="password", type="string", description="Contraseña del usuario"),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request")
     * )
     */
    public function login(Request $request)
    {

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $token = auth()->user()->createToken('AuthToken')->accessToken;

            return response()->json(['token' => $token], 200);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * @OA\Get(
     *     path="/api/tareas",
     *     summary="Muestra una lista de tareas",
     *     tags={"tareas"},
     *     security={{"passport": {}}},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request")
     * )
     */
    public function index()
    {
        $tareas = Tarea::get();

        return response()->json($tareas, 200);
    }

    /**
     * @OA\Post(
     *     path="/api/tareas",
     *     summary="Permite guardar una tarea",
     *     tags={"tareas"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request")
     * )
     */
    public function store(TareaRequest $request)
    {

        $tarea = new Tarea([
            'dni' => $request->dni,
            'titulo' => $request->titulo,
            'descripcion' => $request->descripcion,
            'fecha_vencimiento' => $request->fecha_vencimiento,
            'estado' => $request->estado,
            'creador_id' => Auth::id(),
            'editor_id' => Auth::id()
        ]);

        $tarea->save();

        return response()->json(['message' => 'Tarea creada con éxito'], 201);
    }

    /**
     * @OA\Get(
     *     path="/api/tareas/{{id}}/edit",
     *     summary="Permite editar una tarea",
     *     tags={"tareas"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request")
     * )
     */
    public function edit($id)
    {
 
        $tarea = Tarea::findOrFail($id);

        return response()->json($tarea, 200);
    }

    /**
     * @OA\Put(
     *     path="/api/tareas/{{id}}",
     *     summary="Permite actualizar una tarea",
     *     tags={"tareas"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request")
     * )
     */
    public function update(TareaRequest $request, $id)
    {
        $data = $request->only(['dni', 'titulo', 'descripcion', 'fecha_vencimiento', 'estado']);
        $data['editor_id'] = auth()->user()->id;

        $tarea = Tarea::find($id);
        $tarea->update($data);

        return response()->json(['message' => 'Tarea actualizada con éxito'], 200);
    }

    /**
     * @OA\Delete(
     *     path="/api/tareas/{{id}}",
     *     summary="Permite eliminar una tarea",
     *     tags={"tareas"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request")
     * )
     */
    public function destroy($id)
    {

        $tarea = Tarea::findOrFail($id);
        $tarea->editor_id = auth()->user()->id;
        $tarea->update();

        $tarea->delete();

        return response()->json(['message' => 'Tarea eliminada con éxito'], 200);
    }
}
