<?php

namespace App\Http\Controllers;
use App\Http\Requests\TareaRequest;
use Illuminate\Http\Request;
use App\Models\Tarea;

class TareaController extends Controller
{
    public function index()
    {
        $tareas = Tarea::withTrashed()->orderBy('id', 'desc')->paginate(10);
        return view('admin.tareas.index', compact('tareas'));
    }

    public function create()
    {
        return view('admin.tareas.create');
    }

    public function store(TareaRequest $request)
    {

        $data = $request->only(['dni', 'titulo', 'descripcion', 'fecha_vencimiento', 'estado']);
        $data['creador_id'] = auth()->user()->id;
        $data['editor_id'] = auth()->user()->id;

        Tarea::create($data);

        return redirect()->route('tareas.index')->with('status', 'Tarea creada con éxito');
    }

    public function edit($id)
    {
        $tarea = Tarea::findOrFail($id);

        return view('admin.tareas.edit', compact('tarea'));
    }

    public function update(TareaRequest $request, $id)
    {
        $data = $request->only(['dni', 'titulo', 'descripcion', 'fecha_vencimiento', 'estado']);
        $data['editor_id'] = auth()->user()->id;

        $tarea = Tarea::find($id);
        $tarea->update($data);

        return redirect()->route('tareas.index')->with('status', 'Tarea actualizada con éxito');
    }


    public function destroy($id)
    {
        $tarea = Tarea::findOrFail($id);
        $tarea->editor_id = auth()->user()->id;
        $tarea->update();
        // Soft delete la tarea
        $tarea->delete();

        return redirect()->route('tareas.index')->with('status', 'Tarea eliminada con éxito');
    }
}
