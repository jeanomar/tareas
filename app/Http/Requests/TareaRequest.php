<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TareaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        switch($this->method()){

            case 'POST':
            {
                return [
                    'dni' => 'required|digits:8',
                    'titulo' => 'required|string|max:255',
                    'descripcion' => 'required|string',
                    'fecha_vencimiento' => 'required|date',
                    'estado' => 'required|in:pendiente,en progreso,completada',
                ];
            }
            case 'PUT':
            {
                return [
                    'dni' => 'required|digits:8',
                    'titulo' => 'required|string|max:255',
                    'descripcion' => 'required|string',
                    'fecha_vencimiento' => 'required|date',
                    'estado' => 'required|in:pendiente,en progreso,completada',
                ];
            }
            default:break;
        }
    }
}
