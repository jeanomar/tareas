<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tarea extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'dni',
        'titulo',
        'descripcion',
        'fecha_vencimiento',
        'estado',
        'creador_id',
        'editor_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function creador(){
        return $this->belongsTo(User::class, 'creador_id');
    }

    public function editor(){
        return $this->belongsTo(User::class, 'editor_id');
    }
}
