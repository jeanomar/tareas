@extends('layouts.app_admin')

@section('content')

    <div class="col-12 col-lg-12 col-xxl-10 d-flex">
        <div class="card flex-fill">
            <div class="card-header">
                <h5 class="card-title mb-0">Listado de Tareas</h5>
                
                <div class="row">
                    <div class="col-8"></div>
                    <div class="col-4 text-end">
                        <a class="btn btn-primary" href="{{ route('tareas.create') }}">Nuevo Registro</a>
                    </div>
                </div>           
            </div> 
            <div class="card-body">

                @if(session('status'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        <div class="alert-message">
                            <strong>Mensaje!</strong> {{ session('status') }}!
                        </div>
                    </div>
                @endif
                <div class="table-responsive-sm">
                    <table class="table table-hover table-sm my-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>DNI</th>
                                <th>Título</th>
                                <th>Descripción</th>
                                <th class="d-none d-xl-table-cell">F. Vencimiento</th>
                                <th class="d-none d-xl-table-cell">F. Creación</th>
                                <th>Estado</th>
                                <th class="d-none d-md-table-cell">Ultimo Modificó</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tareas as $tarea)
                            <tr>
                                <td>{{ $tarea->id }}</td>
                                <td>{{ $tarea->dni }}</td>
                                <td>{{ $tarea->titulo }}</td>
                                <td>{{ $tarea->descripcion }}</td>
                                <td class="d-none d-xl-table-cell">{{ date('d-m-Y', strtotime($tarea->fecha_vencimiento)) }}</td>
                                <td class="d-none d-xl-table-cell">{{ date('d-m-Y H:i:s', strtotime($tarea->created_at)) }}</td>
                                <td>
                                    @php
                                        $estadoClass = '';

                                        switch($tarea->estado) {
                                            case 'pendiente':
                                                $estadoClass = 'bg-warning';
                                                break;
                                            case 'en progreso':
                                                $estadoClass = 'bg-info';
                                                break;
                                            case 'completada':
                                                $estadoClass = 'bg-success';
                                                break;
                                            default:
                                                $estadoClass = 'bg-secondary';
                                                break;
                                        }
                                    @endphp
                                    <span class="badge {{ $estadoClass }}">{{ $tarea->estado }}</span> 
                                </td>
                                <td class="d-none d-md-table-cell">{{ ($tarea->editor)?$tarea->editor->name:'' }}</td>
                                <td>
                                    @if (!$tarea->deleted_at)
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
                                        <a href="{{ route('tareas.edit', $tarea->id) }}" class="btn btn-info" title="Editar"><i class="align-middle" data-feather="edit"></i></a>
                                        <a  class="btn btn-danger" title="Eliminar" data-bs-toggle="modal" data-bs-target="#deleteModalLabel{{ $tarea->id }}"><i class="align-middle" data-feather="trash"></i></a>
                                        <!-- Modal -->
                                        <div class="modal fade" id="deleteModalLabel{{ $tarea->id }}" tabindex="-1" style="display: none;" aria-hidden="true">
                                            
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Eliminar Registro</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body m-3">
                                                        <p class="mb-0">Estas seguro de eliminar el Registro #{{ $tarea->id}}.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                        <form action="{{ route('tareas.destroy', $tarea->id) }}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger">Eliminar</button>
                                                        </form>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                           
                                                
                                        </div>
                                    </div>
                                    @else
                                    <p class="text-danger">Eliminado</p>
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
                <div class="text-end mt-3">
                    {{ $tareas->links('vendor.pagination.bootstrap-5') }}
                </div>

            </div>
                
        </div>
    </div>

@endsection
