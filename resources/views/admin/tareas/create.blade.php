@extends('layouts.app_admin')

@section('content')

    <div class="col-12 col-lg-6 col-xxl-6 d-flex">
        <div class="card flex-fill">
            <div class="card-header">
                <h5 class="card-title mb-0">Tareas / Crear Nueva Tarea</h5>    
            </div> 
            <div class="card-body">
                <form action="{{ route('tareas.store') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label for="dni" class="form-label">DNI:</label>
                        <input type="text" name="dni" class="form-control" maxlength="8" minlength="8" pattern="[0-9]+" value="{{ old('dni') }}" required>
                        @error('dni')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="titulo" class="form-label">Título:</label>
                        <input type="text" name="titulo" class="form-control" value="{{ old('titulo') }}" required>
                    </div>

                    <div class="mb-3">
                        <label for="descripcion" class="form-label">Descripción:</label>
                        <textarea name="descripcion" class="form-control" required>{{ old('descripcion') }}</textarea>
                    </div>

                    <div class="mb-3">
                        <label for="fecha_vencimiento" class="form-label">Fecha de Vencimiento:</label>
                        <input type="date" name="fecha_vencimiento" class="form-control" value="{{ old('fecha_vencimiento') }}" required>
                    </div>

                    <div class="mb-3">
                        <label for="estado" class="form-label">Estado:</label>
                        <select name="estado" class="form-select" required>
                            <option value="pendiente" {{ (old('estado') == 'pendiente')?'selected':'' }}>Pendiente</option>
                            <option value="en progreso" {{ (old('estado') == 'en progreso')?'selected':'' }}>En Progreso</option>
                            <option value="completada" {{ (old('estado') == 'completada')?'selected':'' }}>Completada</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary col-12">Guardar Tarea</button>
                </form>

            </div>
        </div>
    </div>
@endsection
