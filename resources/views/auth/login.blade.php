@extends('layouts.app_out')

@section('content')

    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">

                    <div class="card">
                        <div class="card-body">
                            <div class="m-sm-3">
                                <div class="text-center mb-4">
                                   <img src="{{ asset('imgs/tareas.png') }}" style="width: 300px;">
                                   <h3 class="text-info ">{{ config('app.name', 'Laravel') }}</h3>
                                </div>
                                @if ($errors->any())
                                    <div>{{ __('Whoops!') }}</div>
                                    <div class="alert text-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if (session('status'))
                                    <div class="mb-4 font-medium text-sm text-green-600">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label" for="email">{{ __('Email') }}</label>
                                        <input class="form-control form-control-lg" type="email" id="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="username" />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label" for="password">{{ __('Password') }}</label>
                                        <input class="form-control form-control-lg" type="password" id="password" name="password" required autocomplete="current-password"/>
                                    </div>
                                    <div>
                                        <div class="form-check align-items-center">
                                            <input id="customControlInline" type="checkbox" class="form-check-input" value="remember-me" name="remember-me" checked>
                                            <label class="form-check-label text-small" for="customControlInline">{{ __('Remember me') }}</label>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="d-grid gap-2 mt-3">
                                        <button class="btn btn-lg btn-primary" type="submit">{{ __('Login') }}</button>
                                    </div>
                                    @if (Route::has('password.request'))
                                        <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('password.request') }}">
                                            {{ __('Forgot your password?') }}
                                        </a>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mb-3">
                        No tengo una cuenta? <a href="{{ route('register') }}">Registrarme</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection