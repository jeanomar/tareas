@extends('layouts.app_out')

@section('content')

    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">

                    <div class="card">
                        <div class="card-body">
                            <div class="m-sm-3">
                                <div class="text-center mb-4">
                                   <img src="{{ asset('imgs/tareas.png') }}" style="width: 300px;">
                                   <h3 class="text-info ">{{ config('app.name', 'Laravel') }}</h3>
                                </div>
                                @if ($errors->any())
                                    <div>{{ __('Whoops!') }}</div>
                                    <div class="alert text-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label">{{ __('Name') }}</label>
                                        <input class="form-control form-control-lg" type="text" name="name" value="{{ old('name') }}" autocomplete="name" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">{{ __('Email') }}</label>
                                        <input class="form-control form-control-lg" type="email" name="email" value="{{ old('email') }}" autocomplete="username" required/>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">{{ __('Password') }}</label>
                                        <input class="form-control form-control-lg" type="password" name="password" autocomplete="new-password" required/>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">{{ __('Confirm Password') }}</label>
                                        <input class="form-control form-control-lg" type="password" name="password_confirmation" autocomplete="new-password" required/>
                                    </div>
                                    <div class="d-grid gap-2 mt-3">
                                        
                                        <button class="btn btn-lg btn-primary" type="submit">{{ __('Register') }}</button>
                                    </div>
                                </form>

                                @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                                    <div class="mt-4">
                                        <x-label for="terms">
                                            <div class="flex items-center">
                                                <x-checkbox name="terms" id="terms" required />

                                                <div class="ms-2">
                                                    {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                                            'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Terms of Service').'</a>',
                                                            'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Privacy Policy').'</a>',
                                                    ]) !!}
                                                </div>
                                            </div>
                                        </x-label>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="text-center mb-3">
                        <a href="{{ route('login') }}">{{ __('Already registered?') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection