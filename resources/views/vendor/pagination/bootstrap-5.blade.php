@if ($paginator->hasPages())
    <div class="btn-group me-2" role="group" aria-label="First group">
    @if ($paginator->onFirstPage())
        <button type="button" class="btn btn-secondary disabled">@lang('pagination.previous')</button>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" class="btn btn-secondary">@lang('pagination.previous')</a>
    @endif

    @foreach ($elements as $element)
        @if (is_string($element))
            <button type="button" class="btn btn-secondary disabled">{{ $element }}</button>
        @endif

        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <button type="button" class="btn btn-secondary active">{{ $page }}</button>
                @else
                    <a href="{{ $url }}" class="btn btn-secondary">{{ $page }}</a>
                @endif
            @endforeach
        @endif
    @endforeach

    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" class="btn btn-secondary">@lang('pagination.next')</a>
    @else
        <button type="button" class="btn btn-secondary disabled">@lang('pagination.next')</button>
    @endif
</div>

@endif
