<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="index.html">
          <span class="align-middle">{{ config('app.name', 'Laravel') }}</span>
        </a>

        <ul class="sidebar-nav">
            <li class="sidebar-header">
                Menu
            </li>

            <li class="sidebar-item {{ str_contains(request()->route()->getName(), 'dashboard') ? 'active' : '' }}">
                <a class="sidebar-link" href="{{ route('dashboard') }}">
                  <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                </a>
            </li>

            <li class="sidebar-item {{ str_contains(request()->route()->getName(), 'tareas') ? 'active' : '' }}">
                <a class="sidebar-link" href="{{ route('tareas.index') }}">
                  <i class="align-middle" data-feather="book"></i> <span class="align-middle">Tareas</span>
                </a>
            </li>

            <li class="sidebar-item">
                <a class="sidebar-link" href="/api/documentation">
                  <i class="align-middle" data-feather="book"></i> <span class="align-middle">Api Rest</span>
                </a>
            </li>
        </ul>
    </div>
</nav>