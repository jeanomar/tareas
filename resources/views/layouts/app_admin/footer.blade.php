<div class="container-fluid">
    <div class="row text-muted">
        <div class="col-6 text-start">
            <p class="mb-0">
                <strong>Desarrollado por:</strong>
                <a class="text-muted"><strong>{{ config('app.developer') }}</strong></a> &copy;
            </p>
        </div>
        <div class="col-6 text-end">
            <ul class="list-inline">
                <li class="list-inline-item">
                    <a class="text-muted" href="#" target="_blank">Support</a>
                </li>
                
            </ul>
        </div>
    </div>
</div>