@extends('layouts.app_out')

@section('content')

    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">

                    <div class="text-center my-4">
                        <h1 class="h2">Bienvenido a Tareas!</h1>
                        <div class="text-center mb-4">
                           <img src="{{ asset('imgs/tareas.png') }}" style="width: 300px;"> 
                        </div>
                        
                        @auth
                            <a href="{{ url('/dashboard') }}" class="btn btn-success">Dashboard</a>
                        @else
                            <a href="{{ route('login') }}" class="btn btn-danger">Iniciar sesión</a>
                        @endauth
                    </div>

                    <div class="text-center mb-2">
                       No tengo una cuenta? <a href="{{ route('register') }}">Registrarme</a>
                    </div>

                    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                        Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@endsection