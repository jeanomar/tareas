<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TareaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});


// En tu archivo de rutas web.php
Route::middleware('auth')->group(function () {
    Route::resource('tareas', TareaController::class)->names([
        'index' => 'tareas.index',
        'create' => 'tareas.create',
        'store' => 'tareas.store',
        'show' => 'tareas.show',
        'edit' => 'tareas.edit',
        'update' => 'tareas.update',
        'destroy' => 'tareas.destroy',
    ]);
});
