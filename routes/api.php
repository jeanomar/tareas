<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TareaApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', [TareaApiController::class, 'login']);

// En tu archivo de rutas api.php
Route::middleware('auth:api')->group(function () {
    Route::resource('tareas', TareaApiController::class)->names([
        'index' => 'api.tareas.index',
        'create' => 'api.tareas.create',
        'store' => 'api.tareas.store',
        'show' => 'api.tareas.show',
        'edit' => 'api.tareas.edit',
        'update' => 'api.tareas.update',
        'destroy' => 'api.tareas.destroy',
    ]);
});
