<<<<<<< HEAD
# API REST para Tareas

Esta API REST está construida con Laravel y proporciona endpoints para gestionar tareas. A continuación, se detallan los pasos para configurar, instalar y probar la API.

## Requisitos Previos

Asegúrate de tener instalados los siguientes requisitos antes de comenzar:

- [PHP](https://www.php.net/) (versión >= 8.0)
- [Composer](https://getcomposer.org/)
- [MySQL](https://www.mysql.com/) u otro sistema de gestión de bases de datos compatible
- [Postman](https://www.postman.com/) u otra herramienta similar para probar la API

=======
# Tareas


# Características

1. Registrar Usuario
2. Login
3. Crud Tareas con blade
4. Api rest con token para Tareas
5. Las rutas api pueden ser vistas desde Swagger desde una ruta similar :http://localhost:8000/api/documentation#/tareas o genera la documentacion con el siguiente codigo php artisan l5-swagger:generate
6. Tambien pueden ser probados desde Postman:
   -Asegurate de generar el token con la ruta /api/login enviando como body form-data el email y passsword
   - Usa el Bearer token devuelto para las demas rutas del crud /api/tareas ademas configura el header con Content-Type: application/json y X-Requested-With: XMLHttpRequest

# Instalación
Siga estos pasos para instalar la aplicación.

1. Clonar el repositorio
   ```bash
   git clone https://gitlab.com/jeanomar/tareas.git

    Ir al directorio del proyecto

    bash

cd tareas

* Activar extension php extension=sodium

Instalar paquetes con Composer

bash

composer install

Instalar paquetes npm

bash

npm install; npm run dev

Crear su base de datos

Cambiar el nombre de .env.example a .env o copiarlo y pegarlo en el directorio raíz del proyecto y nombrar el archivo .env. También puedes usar este comando.

bash

cp .env.example ./.env

Generar la clave de la aplicación con este comando

bash

php artisan key:generate

Configurar la conexión de la base de datos en el archivo .env

env

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tareas_db
DB_USERNAME=root
DB_PASSWORD=

Importar el archivo completo de la base de datos en la carpeta de la base de datos o ejecutar migraciones. Utilice este comando para ejecutar migraciones.

bash

php artisan migrate

Iniciar el servidor local y abrir la aplicación en el navegador.
Este comando iniciará el servidor de desarrollo.

bash

php artisan serve

Abrir la dirección en el navegador. La dirección suele ser algo así:

text

    http://127.0.0.1:8000

   Credenciales registrate y prueba las funciones



## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jeanomar/tareas/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> fb4c46912060fe092d8dd7bf0496b2fc9a3571e2
